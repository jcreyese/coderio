import UIKit
import Kingfisher


class HomePresenter: HomePresenterProtocol {

    
    weak var view: HomeViewProtocol?
    var interactor: HomeInteractorProtocol?
    var router: HomeRouterProtocol?
	
	
	var booksList: [Book] = []
	var bestSellersList: [String] = []
	var booksBestSellerList: [Book] = []
	var booksHistoryList: [Book] = []
	var booksScienceList: [Book] = []
	var booksBusinessList: [Book] = []
	var genreLists: [String] = []
	var booksTableView: UITableView = UITableView()
	
	
	// MARK: - Service getBestSellers
	func getBestSellersList() {
		self.view?.showLoading()
		interactor?.BestSellersService()
	}
	
	func getBestSellerListSuccess(result: BestSellerData) {
		if let bestSellers = result.results?.best_sellers {
			bestSellersList = bestSellers
		}
		
		self.getBooksList()
	}
	
	func getBestSellerListFailure() {
		self.view?.hideTableView()
		self.view?.hideLoading()
	}
	
	
	// MARK: - Service getBooks
	func getBooksList() {
		self.interactor?.BooksService()
	}

	func getBookListSuccess(result: BookData) {
		if let books = result.results?.books {
			booksList = books
			
			booksBestSellerList = books.filter({ isThisBookABestSeller(book: $0) })
			booksHistoryList = books.filter({ $0.genre == "History" && !isThisBookABestSeller(book: $0) })
			booksScienceList = books.filter({ $0.genre == "Science" && !isThisBookABestSeller(book: $0) })
			booksBusinessList = books.filter({ $0.genre == "Business" && !isThisBookABestSeller(book: $0) })
			
			_ = books.filter({ book in
				if let genre = book.genre {
					!genreLists.contains("Best Sellers") ? genreLists.append("Best Sellers") : nil
					!genreLists.contains(genre) ? genreLists.append(genre) : nil
					return !genreLists.contains(genre)
				} else {
					return false
				}
			})
			
			self.view?.showTableView()
			self.view?.reloadDataTableView()
			self.view?.hideLoading()
		}
	}

	func getBookListFailure() {
		self.view?.hideTableView()
		self.view?.hideLoading()
	}
	
	func isThisBookABestSeller(book: Book) -> Bool {
		for isbn in bestSellersList {
			if isbn == book.isbn {
				return true
			}
		}
		
		return false
	}
	
	
	// MARK: - UITableViewDelegate & UITableViewDataSource
	func numberOfSections() -> Int {
		return genreLists.count
	}
	
	func numberOfRowsInSection(section: Int) -> Int {
		switch section {
			case 0:
				return booksBestSellerList.count
			case 1:
				return booksHistoryList.count
			case 2:
				return booksScienceList.count
			case 3:
				return booksBusinessList.count
			default:
				return 0
		}
		
	}
	
	func cellForRowAt(indexPath: IndexPath, tableView: UITableView) -> UITableViewCell {
		switch indexPath.section {
			case 0:
				return getCell(tableView: tableView, book: booksBestSellerList[indexPath.row])
			case 1:
				return getCell(tableView: tableView, book: booksHistoryList[indexPath.row])
			case 2:
				return getCell(tableView: tableView, book: booksScienceList[indexPath.row])
			case 3:
				return getCell(tableView: tableView, book: booksBusinessList[indexPath.row])
			default:
				return UITableViewCell()
		}
	}
	
	func getCell(tableView: UITableView, book: Book) -> BookTableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "BookTableViewCell") as! BookTableViewCell
		cell.selectionStyle = .none
		cell.bookTitleLabel.text = book.title
		cell.bookAuthorLabel.text = book.author
		
		if let path = book.img {
			let urlString = "\(path)"
			cell.bookImage.kf.indicatorType = .activity
			cell.bookImage.kf.setImage(with: URL(string: urlString), placeholder: UIImage(named: "placeholder"), options: [.transition(.fade(0.5))])
		}
		
		return cell
	}
	
	func viewForHeaderInSection(tableView: UITableView, section: Int) -> UIView? {
		let headerView = UIView()
		headerView.backgroundColor = UIColor(red: 0.89, green: 0.89, blue: 0.89, alpha: 1)
		let sectionLabel = UILabel(frame: CGRect(x: 8, y: 18, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
		sectionLabel.font = UIFont(name: "Play-Bold", size: 16)
		sectionLabel.textColor = UIColor(red: 0.24, green: 0.24, blue: 0.24, alpha: 1)
		
		switch section {
			case 0:
				sectionLabel.text = "Best Sellers - \(booksBestSellerList.count)"
			case 1:
				sectionLabel.text = "History - \(booksHistoryList.count)"
			case 2:
				sectionLabel.text = "Science - \(booksScienceList.count)"
			case 3:
				sectionLabel.text = "Business - \(booksBusinessList.count)"
			default:
				sectionLabel.text = ""
		}
		
		sectionLabel.sizeToFit()
		headerView.addSubview(sectionLabel)
		
		return headerView
	}
	
	func heightForHeaderInSection() -> CGFloat {
		return 40
	}
	
	func getTableView(tableView: UITableView) {
		self.booksTableView = tableView
	}
	
}
