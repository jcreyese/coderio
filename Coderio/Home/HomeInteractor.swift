import UIKit


class HomeInteractor: HomeInteractorProtocol {
    
    
    weak var presenter: HomePresenterProtocol?
    
	
	func BestSellersService() {
		let service = APIRestService(urlServer: "https://raw.githubusercontent.com/ejgteja/files/main/best_sellers.json")
		service.getBestSellers()
		service.completionHandlerBestSeller { [weak self] bestSellers, status, message in
			if status {
				guard let self = self else { return }
				guard let _bestSellers = bestSellers else { return }
				self.presenter?.getBestSellerListSuccess(result: _bestSellers)
			} else {
				self?.presenter?.getBestSellerListFailure()
			}
		}
	}
    
	func BooksService() {
		let service = APIRestService(urlServer: "https://raw.githubusercontent.com/ejgteja/files/main/books.json")
		service.getBooks()
		service.completionHandlerBook { [weak self] books, status, message in
			if status {
				guard let self = self else { return }
				guard let _books = books else { return }
				self.presenter?.getBookListSuccess(result: _books)
			} else {
				self?.presenter?.getBookListFailure()
			}
		}
	}
}
