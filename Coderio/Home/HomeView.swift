import UIKit


class HomeView: UIViewController, HomeViewProtocol {
	
    
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var errorView: UIView!
	@IBOutlet weak var titleErrorLabel: UILabel!
	@IBOutlet weak var descErrorLabel: UILabel!
	@IBOutlet weak var iconErrorImage: UIImageView!
	@IBOutlet weak var reloadButton: UIButton!
	@IBOutlet weak var reloadLabel: UILabel!
	
	var presenter: HomePresenterProtocol?
	
	
	override func viewDidLoad() {
        super.viewDidLoad()
		
		self.title = "Books"
		self.registerCell(tableView: tableView)
		self.setUpTableView()
		self.setUpErrorView()
		self.presenter?.getTableView(tableView: tableView)
		self.presenter?.getBestSellersList()
    }
	
	func registerCell(tableView: UITableView) {
		let identifier = "BookTableViewCell"
		tableView.register(UINib(nibName: identifier, bundle: nil), forCellReuseIdentifier: identifier)
	}
	
	func setUpTableView() {
		self.tableView.delegate = self
		self.tableView.dataSource = self
		self.tableView.estimatedRowHeight = 120.0
	}
	
	func setUpErrorView() {
		self.titleErrorLabel.text = "¡Lo sentimos!"
		self.descErrorLabel.text = "Ha ocurrido un error al recuperar la información, por favor vuelva intentar."
		self.iconErrorImage.image = UIImage(named: "iconError")
		self.reloadButton.layer.cornerRadius = 4
		self.reloadLabel.text = "Reintentar"
	}
	
	@IBAction func reloadButtonAction(_ sender: UIButton) {
		self.presenter?.getBestSellersList()
	}
	
	func showTableView() {
		self.tableView.isHidden = false
		self.errorView.isHidden = true
	}
	
	func hideTableView() {
		self.tableView.isHidden = true
		self.errorView.isHidden = false
	}
	
	
	func reloadDataTableView() {
		self.tableView.reloadData()
	}
	
	func showLoading() {
		self.showSpinner()
	}
	
	func hideLoading() {
		self.hideSpinner()
	}
    
}


// MARK: - UITableViewDelegate & UITableViewDataSource
extension HomeView: UITableViewDelegate, UITableViewDataSource {
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return self.presenter?.numberOfSections() ?? 0
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.presenter?.numberOfRowsInSection(section: section) ?? 0
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		return presenter?.cellForRowAt(indexPath: indexPath, tableView: tableView) ?? UITableViewCell()
	}
	
	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		return presenter?.viewForHeaderInSection(tableView: tableView, section: section)
	}
	
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return presenter?.heightForHeaderInSection() ?? 0
	}
	
}
