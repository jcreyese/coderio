import Foundation
import UIKit


protocol HomeRouterProtocol: AnyObject {
    
}

protocol HomePresenterProtocol: AnyObject {
	func getBooksList()
	func getBookListSuccess(result: BookData)
	func getBookListFailure()
	func getBestSellersList()
	func getBestSellerListSuccess(result: BestSellerData)
	func getBestSellerListFailure()
	func numberOfSections() -> Int
	func numberOfRowsInSection(section: Int) -> Int
	func cellForRowAt(indexPath: IndexPath, tableView: UITableView) -> UITableViewCell
	func viewForHeaderInSection(tableView: UITableView, section: Int) -> UIView?
	func heightForHeaderInSection() -> CGFloat
	func getTableView(tableView: UITableView)
}


protocol HomeInteractorProtocol: AnyObject {
    var presenter: HomePresenterProtocol?  { get set }
	
	func BooksService()
	func BestSellersService()
}


protocol HomeViewProtocol: AnyObject {
    var presenter: HomePresenterProtocol?  { get set }
	
	func reloadDataTableView()
	func showLoading()
	func hideLoading()
	func showTableView()
	func hideTableView()
}
