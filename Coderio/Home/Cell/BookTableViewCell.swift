import UIKit

class BookTableViewCell: UITableViewCell {
	
	
	@IBOutlet weak var bookImage: UIImageView!
	@IBOutlet weak var bookTitleLabel: UILabel!
	@IBOutlet weak var bookAuthorLabel: UILabel!
	

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
