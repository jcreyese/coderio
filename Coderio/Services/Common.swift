import UIKit


fileprivate var loadingView: UIView?


extension UIViewController {
	
	func showSpinner() {
		loadingView = UIView(frame: UIScreen.main.bounds)
		loadingView?.backgroundColor = UIColor(named: "#00000099")
		let spinner = UIActivityIndicatorView()
		spinner.transform = CGAffineTransform(scaleX: 2, y: 2)
		spinner.color = UIColor(named: "#E3E3E3")
		spinner.center = loadingView!.center
		spinner.startAnimating()
		loadingView?.addSubview(spinner)
		self.view.addSubview(loadingView!)
		
	}
	
	func hideSpinner() {
		loadingView?.removeFromSuperview()
		loadingView = nil
	}
	
}
