import Foundation
import Alamofire


class APIRestService {
	
	
	fileprivate var urlServer = ""
	
	typealias bestSellersCallBack = (_ bestSeller: BestSellerData?, _ status: Bool, _ message: String) -> Void
	var bestSellerCallBack: bestSellersCallBack?
	
	typealias booksCallBack = (_ book: BookData?, _ status: Bool, _ message: String) -> Void
	var bookCallBack: booksCallBack?
	
	
	// Init
	init(urlServer: String) {
		self.urlServer = urlServer
	}
	
	
	// GetBestSellersService
	func getBestSellers() {
		let url = "\(self.urlServer)"
		AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil, interceptor: nil, requestModifier: nil).response { (responseData) in
			guard let data = responseData.data else {
				self.bestSellerCallBack?(nil, false, "")
				return
			}
			do {
				let bestSellers = try JSONDecoder().decode(BestSellerData.self, from: data)
				self.bestSellerCallBack?(bestSellers, true, "")
				debugPrint("👨‍💻 NETWORK RESPONSE FOR DEBUG -> \(bestSellers)")
			} catch {
				self.bestSellerCallBack?(nil, false, error.localizedDescription)
				debugPrint("👨‍💻 NETWORK RESPONSE ERROR FOR DEBUG -> \(error.localizedDescription)")
			}
		}
	}
	
	func completionHandlerBestSeller(callBack: @escaping bestSellersCallBack) {
		self.bestSellerCallBack = callBack
	}
	
	
	// GetBooksService
	func getBooks() {
		let url = "\(self.urlServer)"
		AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil, interceptor: nil, requestModifier: nil).response { (responseData) in
			guard let data = responseData.data else {
				self.bookCallBack?(nil, false, "")
				return
			}
			do {
				let books = try JSONDecoder().decode(BookData.self, from: data)
				self.bookCallBack?(books, true, "")
				debugPrint("👨‍💻 NETWORK RESPONSE FOR DEBUG -> \(books)")
			} catch {
				self.bookCallBack?(nil, false, error.localizedDescription)
				debugPrint("👨‍💻 NETWORK RESPONSE ERROR FOR DEBUG -> \(error.localizedDescription)")
			}
		}
	}
	
	func completionHandlerBook(callBack: @escaping booksCallBack) {
		self.bookCallBack = callBack
	}
	
}
