import Foundation


struct BestSellerData: Codable {
	var results: BestSellers?
}

struct BestSellers: Codable {
	var best_sellers: [String]?
}


struct BookData: Codable {
	var results: Books?
}

struct Books: Codable {
	var books: [Book]?
}

struct Book: Codable {
	var isbn: String?
	var title: String?
	var author: String?
	var description: String?
	var genre: String?
	var img: String?
	var imported: Bool?
}
