import UIKit


class SceneDelegate: UIResponder, UIWindowSceneDelegate {
	
	
	var window: UIWindow?
	
	
	func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
		guard let windowScene = (scene as? UIWindowScene) else { return }
		let homeModule = UINavigationController(rootViewController: HomeModule.build())
		window = UIWindow(frame: windowScene.coordinateSpace.bounds)
		window?.makeKeyAndVisible()
		window?.windowScene = windowScene
		window?.rootViewController = homeModule
	}
	
}
